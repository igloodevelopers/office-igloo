﻿using UnityEngine;

public class SelectableObject : MonoBehaviour
{
    public SelectionType type;
    public bool isHoveredOn = false;

    /// <summary>
    /// Send the selection validation to the Popup controller, so it moves to the location provided
    /// and turns on the correct type. 
    /// </summary>
    /// <param name="hitLocation"></param>
    public virtual void IsSelected(Vector3 hitLocation)
    {
        PopupController.Instance.ShowPopup(hitLocation, type);
    }
}
