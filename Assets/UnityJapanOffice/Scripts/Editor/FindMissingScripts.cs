﻿using UnityEngine;
using UnityEditor;
using System.Linq;

public class FindMissingScripts : EditorWindow
{
    [MenuItem("Window/FindMissingScripts")]
    public static void ShowWindow()
    {
        EditorWindow.GetWindow(typeof(FindMissingScripts));
    }

    public void OnGUI()
    {
        if (GUILayout.Button("Find Missing Scripts in selected prefabs"))
        {
            FindInSelected();
        }
    }
    private static void FindInSelected()
    {
        var objs = Selection.gameObjects;
        int missing_count = objs.Sum(GameObjectUtility.RemoveMonoBehavioursWithMissingScript);

        Debug.Log(string.Format("Searched Gameobjects, found {0} missing", missing_count));
    }
}