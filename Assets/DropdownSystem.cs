﻿
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class DropdownSystem : MonoBehaviour
{
    [SerializeField] private int option1DollarCost, option1TimeCost, option1CarbonCost, option1OccupancyChange, option1LuminenceChange, option1AmbientNoiseChange;
    [SerializeField] private int option2DollarCost, option2TimeCost, option2CarbonCost, option2OccupancyChange, option2LuminenceChange, option2AmbientNoiseChange;
    [SerializeField] private int option3DollarCost, option3TimeCost, option3CarbonCost, option3OccupancyChange, option3LuminenceChange, option3AmbientNoiseChange;
    [SerializeField] private int option4DollarCost, option4TimeCost, option4CarbonCost, option4OccupancyChange, option4LuminenceChange, option4AmbientNoiseChange;
    [SerializeField] private Image dropdownParent;

    private int _previousDollarValue = 0;
    private int _previousTimeValue = 0;
    private int _previousCarbonValue = 0;
    private int _previousLuminenceValue = 0;
    private int _previousAmbientNoiseValue = 0;

    private int curSelectedButton = 0;

    public UnityEvent Option1Selected;
    public UnityEvent Option2Selected;
    public UnityEvent Option3Selected;
    public UnityEvent Option4Selected;

    private void Start()
    {
        _previousDollarValue = option1DollarCost;
        _previousTimeValue = option1TimeCost;
        _previousCarbonValue = option1CarbonCost;
    }

    public void SelectButton1(bool isOn) {
        if (!isOn || curSelectedButton == 1) return;
        curSelectedButton = 1;
        dropdownParent.gameObject.SetActive(false);
        Option1Selected.Invoke();
        UpdateCosts(option1DollarCost, option1TimeCost, option1CarbonCost, option1OccupancyChange, option1LuminenceChange, option1AmbientNoiseChange);
    }
    public void SelectButton2(bool isOn) {
        if (!isOn || curSelectedButton == 2) return;
        curSelectedButton = 2;
        dropdownParent.gameObject.SetActive(false);
        Option2Selected.Invoke();
        UpdateCosts(option2DollarCost, option2TimeCost, option2CarbonCost, option2OccupancyChange, option2LuminenceChange, option2AmbientNoiseChange);
    }
    public void SelectButton3(bool isOn) {
        if (!isOn || curSelectedButton == 3) return;
        curSelectedButton = 3;
        dropdownParent.gameObject.SetActive(false);
        Option3Selected.Invoke();
        UpdateCosts(option3DollarCost, option3TimeCost, option3CarbonCost, option3OccupancyChange, option3LuminenceChange, option3AmbientNoiseChange);
    }

    public void SelectButton4(bool isOn)
    {
        if (!isOn || curSelectedButton == 4) return;
        curSelectedButton = 4;
        dropdownParent.gameObject.SetActive(false);
        Option4Selected.Invoke();
        UpdateCosts(option4DollarCost, option4TimeCost, option4CarbonCost, option4OccupancyChange, option4LuminenceChange, option4AmbientNoiseChange);
    }

    private void UpdateCosts(int dollar, int time, int carbon, int seating, int lux, int db)
    {
        // Remove previous costs
        Igloo.CostManager.Instance.NewDollarCostAdjustment(-_previousDollarValue);
        Igloo.CostManager.Instance.NewTimeCostAdjustment(-_previousTimeValue);
        Igloo.CostManager.Instance.NewCarbonCostAdjustment(-_previousCarbonValue);
        Igloo.CostManager.Instance.NewLuminoictyAdjustment(-_previousLuminenceValue);
        Igloo.CostManager.Instance.NewAmbientNoiseAdjustment(-_previousAmbientNoiseValue);

        // Update new value
        Igloo.CostManager.Instance.NewDollarCostAdjustment(dollar);
        Igloo.CostManager.Instance.NewTimeCostAdjustment(time);
        Igloo.CostManager.Instance.NewCarbonCostAdjustment(carbon);
        Igloo.CostManager.Instance.NewLuminoictyAdjustment(lux);
        Igloo.CostManager.Instance.NewAmbientNoiseAdjustment(db);
        if (seating != 0) Igloo.CostManager.Instance.NewOccupancydjustment(seating);

        // Cache new value
        _previousDollarValue = dollar;
        _previousTimeValue = time;
        _previousCarbonValue = carbon;
        _previousLuminenceValue = lux;
        _previousAmbientNoiseValue = db;
    }
}
