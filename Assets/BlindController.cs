﻿using System.Collections;
using UnityEngine;

public class BlindController : MonoBehaviour
{
    [SerializeField] private Transform blind;
    [SerializeField] private Vector3 blindScale1, blindScale2, blindScale3;
    [SerializeField] private Vector3 blindPosition1, blindPosition2, blindPosition3;

    private readonly float duration = 3.0f;
    private Vector3 previousScale = Vector3.zero;
    private Vector3 previousPosition = Vector3.zero;

    private void Start()
    {
        previousPosition = blindPosition1;
        previousScale = blindScale1;
    }

    public void AdjustBlind(int positionID)
    {
        StopAllCoroutines();
        switch (positionID)
        {
            case 1:
                StartCoroutine(Lerp(blindPosition1, blindScale1));
                break;
            case 2:
                StartCoroutine(Lerp(blindPosition2, blindScale2));
                break;
            case 3:
                StartCoroutine(Lerp(blindPosition3, blindScale3));
                break;

            default: goto case 1;
        }
    }

    private IEnumerator Lerp(Vector3 NewPos, Vector3 NewScale)
    {
        float timeElapsed = 0;

        while (timeElapsed < duration)
        {
            blind.localPosition = Vector3.Lerp(previousPosition, NewPos,  timeElapsed / duration);
            blind.localScale = Vector3.Lerp(previousScale, NewScale,  timeElapsed / duration);
            timeElapsed += Time.deltaTime;

            yield return new WaitForEndOfFrame() ;
        }

        previousPosition = NewPos;
        previousScale = NewScale;
    }
}
