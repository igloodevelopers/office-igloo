﻿using UnityEngine;
using System.Collections;
using UnityEngine.Rendering.HighDefinition;

public class EnvironmentLightingController : MonoBehaviour
{
    [SerializeField] private Light[] lights;
    [SerializeField] private MeshRenderer environmentRend;
    [System.Serializable]
    public struct TimeOfDay
    {
        public string name;
        public float intensity;
        public float colourTemperature;
        public Material environmentMat; 
    }

    [SerializeField] public TimeOfDay[] timeOfDay;

    public void SetTimeOfDay(int ID)
    {
        switch (ID)
        {
            case 1:
                Debug.Log($"Setting to {timeOfDay[0].name}");
                SetLightParams(
                    timeOfDay[0].colourTemperature,
                    timeOfDay[0].intensity,
                    timeOfDay[0].environmentMat
                );
                break;
            case 2:
                Debug.Log($"Setting to {timeOfDay[1].name}");
                SetLightParams(
                    timeOfDay[1].colourTemperature,
                    timeOfDay[1].intensity,
                    timeOfDay[1].environmentMat
                );
                break;
            case 3:
                Debug.Log($"Setting to {timeOfDay[2].name}");
                SetLightParams(
                    timeOfDay[2].colourTemperature,
                    timeOfDay[2].intensity,
                    timeOfDay[2].environmentMat
                );
                break;


            default: goto case 1;
        }
    }


    private void SetLightParams(float newColorTemp, float newIntensity, Material newEnvironmentMat)
    {
        foreach (Light light in lights)
        {
            light.colorTemperature = newColorTemp;
            light.GetComponent<HDAdditionalLightData>().intensity = newIntensity;
        }
        environmentRend.material = newEnvironmentMat;
    }
}
