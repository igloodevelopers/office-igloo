﻿using UnityEngine;

public class AdjustAudioReverbZone : MonoBehaviour
{
    public void AdjustAudioReverb(string zoneName)
    {
        AudioReverbPreset.TryParse(zoneName, out AudioReverbPreset preset);
        GetComponent<AudioReverbZone>().reverbPreset = preset;
    }
}
