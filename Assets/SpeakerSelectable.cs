﻿using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public class SpeakerSelectable : SelectableObject
{
    public AudioSource[] speakerOutputs;

    public void TurnOffSpeakers()
    {
        foreach(AudioSource spkr in speakerOutputs)
        {
            spkr.Stop();
        }
    }

    public void PlayAudio(AudioClip clip)
    {
        foreach (AudioSource spkr in speakerOutputs)
        {
            spkr.clip = clip;
            spkr.Play();
        }
    }
}
