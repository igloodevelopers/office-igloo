﻿using System;
using UnityEngine;
using UnityEngine.UI;
namespace Igloo
{
    public class CostManager : MonoBehaviour
    {
        #region Instance
        private static CostManager instance;
        public static CostManager Instance
        {
            get
            {
                if (instance == null) instance = FindObjectOfType<CostManager>();
                return instance;
            }
        }
        #endregion

        #region Cost Variables

        private int _dollarCost = 0;
        [Header("Dollar Cost"), SerializeField] private int _initialDollarCost = 0;
        [HideInInspector] public int DollarCost
        {
            get { return _dollarCost; }
            set
            {
                _dollarCost = value;
                AdjustDollarCost();
            }
        }
    
        [SerializeField] private Text[] DollarCostText;

        private int _timeCost = 0;
        [Header("Time Cost"), SerializeField] private int _initialTimeCost = 0;
        [HideInInspector] public int TimeCost
        {
            get { return _timeCost; }
            set
            {
                _timeCost = value;
                AdjustTimeCost();
            }
        }
        [SerializeField] private Text[] TimeCostText;

        private int _carbonCost = 0;
        [Header("Carbon Cost"), SerializeField] private int _initialCarbonCost = 0;
        [HideInInspector] public int CarbonCost
        {
            get { return _carbonCost; }
            set
            {
                _carbonCost = value;
                AdjustCarbonCost();
            }
        }
        [SerializeField] private Text[] CarbonCostText;


        private int _seatingAvailability = 0;
        [Header("Seating Availability"), SerializeField] private int _initialOccupancy = 30;
        [HideInInspector] public int SeatingAvailability
        {
            get { return _seatingAvailability; }
            set
            {
                _seatingAvailability = value;
                AdjustSeatingAvailability();
            }
        }
        [SerializeField] private Text[] SeatingAvailabilityText;


        private int _luminocity = 0;
        [Header("Luminocity"), SerializeField] private int _initialLuminocity = 30;
        [HideInInspector]
        public int Luminocity
        {
            get { return _luminocity; }
            set
            {
                _luminocity = value;
                AdjustLuminocity();
            }
        }

        [SerializeField] private Text[] LuminocityText;

        private int _ambientNoise = 0;
        [Header("Ambient Noise"), SerializeField] private int _initialAmbientNoise = 30;
        [HideInInspector]
        public int AmbientNoise
        {
            get { return _ambientNoise; }
            set
            {
                _ambientNoise = value;
                AdjustNoise();
            }
        }

        [SerializeField] private Text[] AmbientNoiseText;


        #endregion

        private void Start()
        {
            DollarCost = _initialDollarCost;
            TimeCost = _initialTimeCost;
            CarbonCost = _initialCarbonCost;
            SeatingAvailability = _initialOccupancy;
            Luminocity = _initialLuminocity;
            AmbientNoise = _initialAmbientNoise;
        }

        #region UI Adjustments
        private void AdjustDollarCost()
        {
            foreach (Text txt in DollarCostText)
            {
                txt.text = $"Cost: ${_dollarCost}";
            }
        }

        private void AdjustTimeCost()
        {
            foreach (Text txt in TimeCostText)
            {
                txt.text = $"Build Time: {_timeCost} Days";
            }
        }

        private void AdjustCarbonCost()
        {
            foreach (Text txt in CarbonCostText)
            {
                txt.text = $"Emissions: {_carbonCost} kgCO2";
            }
        }

        private void AdjustSeatingAvailability()
        {
            foreach (Text txt in SeatingAvailabilityText)
            {
                txt.text = $"Max Occupancy: {_seatingAvailability}";
            }
        }

        private void AdjustLuminocity()
        {
            foreach (Text txt in LuminocityText)
            {
                txt.text = $"Luminocity: {_luminocity} Lux";
            }
        }

        private void AdjustNoise()
        {
            foreach (Text txt in AmbientNoiseText)
            {
                txt.text = $"Ambient Noise: {_ambientNoise} dB";
            }
        }
        #endregion

        /// <summary>
        /// Always adjust the dollar cost value, by removing the previous value, and then adding then new value. 
        /// To remove a value, send a negative number.
        /// </summary>
        /// <param name="value"></param>
        public void NewDollarCostAdjustment(int value)
        {
            DollarCost += value;
        }

        /// <summary>
        /// Always adjust the time cost value, by removing the previous value, and then adding then new value. 
        /// To remove a value, send a negative number.
        /// </summary>
        /// <param name="value"></param>
        public void NewTimeCostAdjustment(int value)
        {
            TimeCost += value;
        }

        /// <summary>
        /// Always adjust the carbon cost value, by removing the previous value, and then adding then new value. 
        /// To remove a value, send a negative number.
        /// </summary>
        /// <param name="value"></param>
        public void NewCarbonCostAdjustment(int value)
        {
            CarbonCost += value;
        }

        /// <summary>
        /// The value parameter is a direct occupancy adjustment
        /// So if the new size is 30, input 30
        /// </summary>
        /// <param name="value"></param>
        public void NewOccupancydjustment(int value)
        {
            SeatingAvailability = value;
        }

        /// <summary>
        /// Always adjust the luminocity value, by removing the previous value, and then adding then new value. 
        /// To remove a value, send a negative number.
        /// </summary>
        /// <param name="value"></param>
        public void NewLuminoictyAdjustment(int value)
        {
            Luminocity += value;
        }

        /// <summary>
        /// Always adjust the noise value, by removing the previous value, and then adding then new value. 
        /// To remove a value, send a negative number.
        /// </summary>
        /// <param name="value"></param>
        public void NewAmbientNoiseAdjustment(int value)
        {
            AmbientNoise += value;
        }
    }
}

