﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioListener))]
public class AudioMeter : MonoBehaviour
{
    public float CurrentAudioLevel = 0.0f;
    private AudioListener listener = null;

    private void Start()
    {
        listener = GetComponent<AudioListener>();
        InvokeRepeating("TestAudioAtLocation", 1.0f, 1.0f);
    }

    private void TestAudioAtLocation()
    {
        
    }
}
