﻿using UnityEngine;
using UnityEngine.Rendering.HighDefinition;

public class LuminanceController : MonoBehaviour
{
    [SerializeField] private RenderTexture rt;
    public int LuminanceValue = 0;
    private Texture2D tex;
    [SerializeField] private Light sun;

    void Start()
    {
        tex = new Texture2D(2, 2 ,TextureFormat.RGB24, false);
        GetComponent<Camera>().targetDisplay = -1;
        InvokeRepeating("TestLighting", 5f, 5f);
    }

    void TestLighting()
    {
        RenderTexture.active = rt;
        tex.ReadPixels(new Rect(0, 0, rt.width, rt.height), 0, 0);
        rt.Release();
        tex.Apply();
        
        float gsLux = tex.GetPixel(0, 0).grayscale;
        float sunIntensity = sun.GetComponent<HDAdditionalLightData>().intensity;
        Igloo.CostManager.Instance.NewLuminoictyAdjustment( (int)(gsLux * sunIntensity));
        
    }

    public void ApplyNewAmbientNoiseSetting(int AmbientNoiseValue)
    {
        Igloo.CostManager.Instance.NewAmbientNoiseAdjustment(AmbientNoiseValue);
    }
}
