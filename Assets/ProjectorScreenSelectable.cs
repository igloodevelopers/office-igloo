﻿using UnityEngine;

public class ProjectorScreenSelectable : SelectableObject
{
    private bool screenDown = true;
    private Animator anim = null;

    private void Start()
    {
        if (GetComponent<Animator>()) anim = GetComponent<Animator>();
    }

    private void Update()
    {
        if (Input.GetButtonDown("Fire3") && isHoveredOn && type == SelectionType.Projector)
        {
            if (!anim) return;
            ToggleProjectorScreen();
        }
    }

    private void ToggleProjectorScreen()
    {
        anim.SetTrigger(screenDown ? "ScreenUp" : "ScreenDown");
        screenDown = !screenDown;
    }
}
