﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace CurvedUI
{
    /// <summary>
    /// This class contains code that controls the visuals (only!) of the laser pointer.
    /// </summary>
    public class CurvedUILaserBeam : MonoBehaviour
    {
#pragma warning disable 0649
        [SerializeField]
        Transform LaserBeamTransform;
        [SerializeField]
        Transform LaserBeamDot;
        [SerializeField]
        bool CollideWithMyLayerOnly = false;
        [SerializeField]
        bool hideWhenNotAimingAtCanvas = false;
        public Vector3 testPointAdjustment;
        private SelectableObject curGazedObject;

#pragma warning restore 0649

        // Update is called once per frame
        protected void Update()
        {
            if (Input.GetButtonDown("Fire2"))
            {
                PopupController.Instance.HidePopup();
            }
            //get direction of the controller
            Ray myRay = new Ray(this.transform.position, this.transform.forward);


            //make laser beam hit stuff it points at.
            if(LaserBeamTransform && LaserBeamDot) {
                //change the laser's length depending on where it hits
                float length = 10000;


                //create layerMaskwe're going to use for raycasting
                int myLayerMask = -1;
                if (CollideWithMyLayerOnly)
                {
                    //lm with my own layer only.
                    myLayerMask = 1 << this.gameObject.layer;
                }


                if (Physics.Raycast(myRay, out RaycastHit hit, length, myLayerMask))
                {
                    CurvedUISettings cuiSettings = hit.collider.GetComponentInParent<CurvedUISettings>();

                    length = Vector3.Distance(hit.point, this.transform.position);

                    //Find if we hit a canvas
                    if (cuiSettings != null)
                    {
                        //find if there are any canvas objects we're pointing at. we only want transforms with graphics to block the pointer. (that are drawn by canvas => depth not -1)
                        int selectablesUnderPointer = cuiSettings.GetObjectsUnderPointer().FindAll(x => x != null && x.GetComponent<Graphic>() != null && x.GetComponent<Graphic>().depth != -1).Count;

                        length = selectablesUnderPointer == 0 ? 10000 : Vector3.Distance(hit.point, this.transform.position);
                        CurvedUIInputModule.CustomControllerRay = myRay;
                        CurvedUIInputModule.CustomControllerButtonState = Input.GetButtonDown("Fire1");
                    }
                    else if (hit.collider.gameObject.GetComponent<SelectableObject>())
                    {
                        if (curGazedObject != null) curGazedObject.isHoveredOn = false;
                        curGazedObject = hit.collider.gameObject.GetComponent<SelectableObject>();
                        curGazedObject.isHoveredOn = true;
                        Vector3 testPoint = (hit.point + hit.normal) + testPointAdjustment;
                        if (Input.GetButtonDown("Fire1") && cuiSettings == null)
                        {
                            curGazedObject.IsSelected(testPoint);
                        }
                    }
                    else if (hideWhenNotAimingAtCanvas) length = 0;
                }
                else if (hideWhenNotAimingAtCanvas) length = 0;


                //set the leangth of the beam
                LaserBeamTransform.localScale = LaserBeamTransform.localScale.ModifyZ(length);
            }
           

        }
    }
}
