﻿using UnityEngine;
using System.Collections;
using Igloo.Common;

    public class OfficePlayerPointer : PlayerPointer
    {
        private SelectableObject curGazedObject;
        private float maxTimeNoInteraction = 10.0f;
        public override void Update()
        {
            base.Update();
            //CurvedUIInputModule.CustomControllerRay = new Ray(transform.position, transform.TransformDirection(Vector3.forward * 10));
            //Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward * 10), Color.blue);
            //if (Input.GetButtonDown("Fire1"))
            //{
            //    CurvedUIInputModule.CustomControllerButtonState = true;
            //    layerMask = LayerMask.GetMask("Default");
            //}
            //CurvedUIInputModule.CustomControllerButtonState = false;

            if (hit.collider)
            {
                if (hit.collider.gameObject.GetComponent<SelectableObject>())
                {
                    if (curGazedObject != null) curGazedObject.isHoveredOn = false;
                    curGazedObject = hit.collider.gameObject.GetComponent<SelectableObject>();
                    curGazedObject.isHoveredOn = true;
                    if (Input.GetButtonDown("Fire1"))
                    {
                        // Send the hit location point, and confirm that it is selected. 
                        curGazedObject.IsSelected(hit.point);
                        //layerMask = LayerMask.GetMask("UI");
                        //StartCoroutine(WaitForNoInteraction());
                    }
                }
            }
        }

        private IEnumerator WaitForNoInteraction()
        {
            yield return new WaitForSeconds(PopupController.Instance.maxTimeNoInteraction);
            //layerMask = LayerMask.GetMask("Default");
        }
    }


