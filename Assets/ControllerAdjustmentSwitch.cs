﻿using UnityEngine;
using UnityEngine.EventSystems;

public class ControllerAdjustmentSwitch : MonoBehaviour
{
    public StandaloneInputModule standAloneInput;
    public CurvedUIInputModule CurvedUIInputModule;
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (!standAloneInput.enabled)
            {
                CurvedUIInputModule.enabled = false;
                standAloneInput.enabled = true;
            }
        }
        if (Input.GetButtonDown("Fire1"))
        {
            if (!CurvedUIInputModule.enabled)
            {
                standAloneInput.enabled = false;
                CurvedUIInputModule.enabled = true;
            }
        }
    }
}
