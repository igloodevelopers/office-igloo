﻿using System.Collections;
using UnityEngine.UI;
using UnityEngine;
using System;

namespace Igloo
{
    public class MS_OfficeSelector : MonoBehaviour
    {
        [Header("User Interface")]
        [SerializeField] private Vector3 canvasOffset = Vector3.zero;
        [SerializeField] private float canvasScaleOffset = 0.0f;
        [SerializeField] private Canvas canvas;
        [SerializeField] private GameObject buttonPrefab;
        [Header("Current Object")]
        private readonly string currentObject;
        private readonly GameObject currentPrefab;
        private readonly float currentFinanceCost, currentCarbonCost, currentInstallationTime;

        [System.Serializable]
        public struct Object
        {
            [Tooltip("Must be Unique")]
            public string name;
            [Tooltip("Don't use the model direct from modelling program, create a prefab")]
            public GameObject prefab;
            [Tooltip("Make sure it has transparency enabled")]
            public Sprite thumb;
            [Tooltip("Use rough estimates")]
            public float financeCost, carbonCost, installationTime;
            [Tooltip("Should this be spawned as the first object?")]
            public bool isFirstObject;
        }
        [Header("Object Params")]
        public Object[] objects;

        private void Start()
        {
            PopulateCanvas();
            HideCanvas();
        }

        private void PopulateCanvas()
        {
            for(int i = 0; i < objects.Length; i++)
            {
                Button btn = Instantiate(buttonPrefab).GetComponent<Button>();
                btn.image.sprite = objects[i].thumb;

                // check if is first Object
                if (objects[i].isFirstObject) SpawnObject(objects[i].prefab);
            }
        }

        private void SpawnObject(GameObject prefab)
        {
            GameObject obj = Instantiate(prefab, this.transform);
            obj.transform.localPosition = Vector3.zero;
        }

        private void HideCanvas()
        {
            canvas.gameObject.SetActive(false);
        }

        private void ShowCanvas()
        {
            canvas.gameObject.SetActive(true);
            canvas.transform.LookAt(Camera.main.transform);
        }
    }
}

