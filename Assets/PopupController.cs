﻿using UnityEngine;
using System.Collections;

public class PopupController : MonoBehaviour
{
    public GameObject[] ButtonGroups;
    public Vector3 PositionOffset = new Vector3(0, 0, 0);
    public float maxTimeNoInteraction = 10.0f;
    private Vector3 hidePosition = new Vector3(0, -1, 0);

    private static PopupController instance;
    public static PopupController Instance
    {
        get
        {
            if (instance == null) instance = FindObjectOfType<PopupController>();
            return instance;
        }
    }

    public void ShowPopup(Vector3 location, SelectionType selectonType)
    {
        StopAllCoroutines();
        // Turn everything off, just encase.
        for(int i = 0; i < ButtonGroups.Length; i++) { ToggleEverything(ButtonGroups[i], false); }
        switch (selectonType)
        {
            case SelectionType.Projector:
                ToggleEverything(ButtonGroups[0],true);
                break;
            case SelectionType.Furniture:
                ToggleEverything(ButtonGroups[1],true);
                break;
            case SelectionType.Ceiling:
                ToggleEverything(ButtonGroups[2],true);
                break;
            case SelectionType.Flooring:
                ToggleEverything(ButtonGroups[3],true);
                break;
            case SelectionType.Blinds:
                ToggleEverything(ButtonGroups[4],true);
                break;
            case SelectionType.Paint:
                ToggleEverything(ButtonGroups[5],true);
                break;
            case SelectionType.Lighting:
                ToggleEverything(ButtonGroups[6],true);
                break;
            case SelectionType.Speaker:
                ToggleEverything(ButtonGroups[7], true);
                break;
        }
        // Move to the correct location
        this.transform.position = location;
        this.transform.LookAt(Camera.main.transform);
        // Turn everything on
        StartCoroutine(WaitForNoInteraction());
    }

    private IEnumerator WaitForNoInteraction()
    {
        yield return new WaitForSeconds(maxTimeNoInteraction);
        HidePopup();
    }

    public void HidePopup()
    {
        StopAllCoroutines();
        this.transform.position = hidePosition;
    }

    void ToggleEverything(GameObject parent, bool show)
    {
        parent.transform.localPosition = show ? new Vector3(-400,200,0) : new Vector3(0, 10000, 0);
    }
}

public enum SelectionType
{
    Projector, Furniture, Ceiling, Blinds, Lighting, Paint, Flooring, Speaker
}
